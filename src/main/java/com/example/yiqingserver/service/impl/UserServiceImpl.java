package com.example.yiqingserver.service.impl;

import cn.hutool.cache.Cache;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.yiqingserver.entity.User;
import com.example.yiqingserver.mapper.UserMapper;
import com.example.yiqingserver.req.LoginReq;
import com.example.yiqingserver.service.UserService;
import com.example.yiqingserver.utils.Result;
import org.apache.ibatis.mapping.CacheBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public Result login(LoginReq loginReq) {
        QueryWrapper wrapper = new QueryWrapper<>();
        wrapper.eq("username",loginReq.getUsername());
        final User user = userMapper.selectOne(wrapper);
        if(user == null){
            return new Result().setCode(401).setMsg("用户名不存在").setData(null);
        }else{
            if(user.getUsername().equals(loginReq.getUsername()) && user.getPassword().equals(loginReq.getPassword())){
                return new Result<>(200,"登录成功",user);
            }else if(user.getUsername().equals(loginReq.getUsername()) && !user.getPassword().equals(loginReq.getPassword())){
                return new Result<>(401,"密码错误",null);
            }else{
                return new Result<>(401,"用户名错误",null);
            }
        }
    }
}
