package com.example.yiqingserver.service.impl;

import com.example.yiqingserver.entity.Nuclein;
import com.example.yiqingserver.mapper.NucleinMapper;
import com.example.yiqingserver.service.NucleinService;
import com.example.yiqingserver.utils.Result;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class NucleinServiceImpl implements NucleinService {

    @Resource
    private NucleinMapper nucleinMapper;

    /**
     * 上传核酸信息给学院
     * @param nuclein
     * @return
     */
    @Override
    public Result uploadToAcademy(Nuclein nuclein) {
        nuclein.setStatus(false);
        nucleinMapper.insert(nuclein);
        return new Result<>(200,"上传成功",nuclein);
    }
}
