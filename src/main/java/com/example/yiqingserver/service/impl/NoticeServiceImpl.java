package com.example.yiqingserver.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.yiqingserver.entity.Notice;
import com.example.yiqingserver.mapper.NoticeMapper;
import com.example.yiqingserver.service.NoticeService;
import com.example.yiqingserver.utils.Result;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class NoticeServiceImpl implements NoticeService {

    @Resource
    private NoticeMapper noticeMapper;

    @Override
    public Result publishNotice(Notice notice) {
        QueryWrapper wrapper = new QueryWrapper<Notice>();
        wrapper.eq("title",notice.getTitle());
        final List<Notice> list = noticeMapper.selectList(wrapper);
        if(!ObjectUtils.isEmpty(list)){
            return new Result<>(401,"标题重复",null);
        }else {
            noticeMapper.insert(notice);
            return new Result<>(200,"成功发布一条通知",notice);
        }
    }
}
