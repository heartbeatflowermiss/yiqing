package com.example.yiqingserver.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.yiqingserver.entity.Files;
import com.example.yiqingserver.mapper.FileMapper;
import com.example.yiqingserver.service.FileService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.List;

@Service
public class FileServiceImpl implements FileService {

    @Value("${files.upload.path}")
    private String fileUploadPath;
    @Resource
    private FileMapper fileMapper;
    @Override
    public String upload(MultipartFile file) throws IOException {
        String originalFilename = file.getOriginalFilename();
        String type = FileUtil.extName(originalFilename);
        Long size = file.getSize();
        //定义文件唯一的标识
        String uuid = IdUtil.fastSimpleUUID();
        String fileUUID = uuid + StrUtil.DOT + type;

        File uploadFile = new File(fileUploadPath + fileUUID);

        //判断文件目录是否存在，如果不存在则创建
        File parentFile = uploadFile.getParentFile();
        if (!parentFile.exists()) {
            parentFile.mkdirs();
        }
        String fileMd5;
        String fileUrl;
        file.transferTo(uploadFile);
        // 获取文件的md5
        fileMd5 = SecureUtil.md5(uploadFile);
        Files dbFiles = getFileMd5(fileMd5);
        if (dbFiles != null) {
            fileUrl = dbFiles.getFileUrl();
            uploadFile.delete();
        } else {
            //如果不存在，则不删除刚才上传的文件
            fileUrl = "http://localhost:8080/sys-file/" + fileUUID;
        }

        //再存入数据库
        Files saveFile = new Files();
        saveFile.setFileName(originalFilename);
        saveFile.setFileType(type);
        saveFile.setFileSize(size / 1024);
        saveFile.setFileUrl(fileUrl);
        saveFile.setFileMd5(fileMd5);
        fileMapper.insert(saveFile);
        return fileUrl;
    }

    /**
     * 通过md5查询文件
     *
     * @param fileMd5
     * @return
     */
    private Files getFileMd5(String fileMd5) {
        //        查询文件是否存在
        QueryWrapper wrapper = new QueryWrapper<>();
        wrapper.eq("file_md5", fileMd5);
        List<Files> filesList = fileMapper.selectList(wrapper);
        return filesList.size() == 0 ? null : filesList.get(0);
    }
}
