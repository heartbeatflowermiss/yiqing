package com.example.yiqingserver.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.yiqingserver.entity.Leave;
import com.example.yiqingserver.mapper.LeaveMapper;
import com.example.yiqingserver.service.LeaveService;
import com.example.yiqingserver.utils.Result;
import com.example.yiqingserver.utils.TimeUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Service
public class LeaveServiceImpl implements LeaveService {

    @Resource
    private LeaveMapper leaveMapper;
    @Override
    public Result entry(Leave leave) {
        leave.setStatus(0);
        TimeUtils timeUtils = new TimeUtils();
        String date = timeUtils.backTime();
        leave.setLeaveTime(date);
        leaveMapper.insert(leave);
        return new Result<>(200,"登记成功",leave);
    }

    @Override
    public Result leaveCensus() {
        QueryWrapper wrapper1 = new QueryWrapper<Leave>();
        wrapper1.eq("status",0);
        QueryWrapper wrapper2 = new QueryWrapper<Leave>();
        wrapper2.eq("status",1);
        QueryWrapper wrapper3 = new QueryWrapper<Leave>();
        wrapper3.eq("status",2);
//        出入总数
        final Long total = leaveMapper.selectCount(null);
//        待审核
        final Long todoCount = leaveMapper.selectCount(wrapper1);
//        审核中
        final Long doingCount = leaveMapper.selectCount(wrapper2);
//        已审核
        final Long doneCount = leaveMapper.selectCount(wrapper3);

        Map<String,Long> map = new HashMap<>();
        map.put("出入总数",total);
        map.put("待审核数",todoCount);
        map.put("审核中数",doingCount);
        map.put("已审核数",doneCount);
        return new Result<>(200,"这是出入统计数据",map);
    }

    @Override
    public Result updateLeave(Leave leave) {
        leaveMapper.updateLeave(leave);
        return new Result<>(200,"修改成功",leave);
    }
}
