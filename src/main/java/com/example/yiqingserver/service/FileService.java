package com.example.yiqingserver.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileService {

    public String upload(MultipartFile file) throws IOException;
}
