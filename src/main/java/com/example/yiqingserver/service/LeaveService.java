package com.example.yiqingserver.service;

import com.example.yiqingserver.entity.Leave;
import com.example.yiqingserver.utils.Result;

public interface LeaveService {

    public Result entry(Leave leave);

    public Result leaveCensus();

    public Result updateLeave(Leave leave);
}
