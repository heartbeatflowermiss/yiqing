package com.example.yiqingserver.service;

import com.example.yiqingserver.entity.Notice;
import com.example.yiqingserver.utils.Result;

public interface NoticeService {

    public Result publishNotice(Notice notice);
}
