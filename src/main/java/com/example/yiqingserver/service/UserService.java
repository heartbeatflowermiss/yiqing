package com.example.yiqingserver.service;

import com.example.yiqingserver.req.LoginReq;
import com.example.yiqingserver.utils.Result;

public interface UserService {

    public Result login(LoginReq loginReq);
}
