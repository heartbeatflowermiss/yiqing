package com.example.yiqingserver.service;

import com.example.yiqingserver.entity.Health;
import com.example.yiqingserver.req.HealthReq;
import com.example.yiqingserver.utils.Result;

import javax.annotation.Resource;

public interface HealthService {

    @Resource
    public Result clockIn(Health health);
}
