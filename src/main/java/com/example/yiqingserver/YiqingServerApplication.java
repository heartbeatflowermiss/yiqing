package com.example.yiqingserver;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.example.yiqingserver.mapper")
public class YiqingServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(YiqingServerApplication.class, args);
    }

}
