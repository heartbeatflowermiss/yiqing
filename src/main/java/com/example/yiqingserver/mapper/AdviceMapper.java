package com.example.yiqingserver.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.yiqingserver.entity.Advice;
import com.example.yiqingserver.req.FeedBackReq;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface AdviceMapper extends BaseMapper<Advice> {

    public void receive(Integer id);

    public void feedBack(FeedBackReq feedBackReq);

    public List<Advice> getAdviceById(Integer uid);

    public List<Advice> getAllAdvice();
}
