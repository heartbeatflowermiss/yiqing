package com.example.yiqingserver.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.yiqingserver.entity.Area;
import com.example.yiqingserver.req.IsClearance;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface AreaMapper extends BaseMapper<Area> {

    public void updateArea(Area area);

    public void isClearance(IsClearance isClearance);

    public List<Area> getAreaList();

    public List<Area> controlling();
}
