package com.example.yiqingserver.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.yiqingserver.entity.Nuclein;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface NucleinMapper extends BaseMapper<Nuclein> {

    public void uploadToSchool(Integer id);

    public List<Nuclein> getNucleinById(Integer uid);

    public List<Nuclein> getAcademyNuc(String academy);

    public List<Nuclein> getNucleinByStatus();
}
