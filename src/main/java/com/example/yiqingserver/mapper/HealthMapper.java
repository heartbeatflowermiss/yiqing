package com.example.yiqingserver.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.yiqingserver.entity.Health;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface HealthMapper extends BaseMapper<Health> {

    public void uploadToSchool(Integer id);

    public List<Health> getHealth(Integer uid);

    public List<Health> getHealthBystatus();

    public List<Health> allClockIn();

    public List<Health> showClockIn(String academy);
}
