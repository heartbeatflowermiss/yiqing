package com.example.yiqingserver.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("tb_area")
public class Area {

    @TableId(type = IdType.AUTO)
    private Integer id;
    //    区域名称
    private String areaName;
    //    经度
    private Double longitude;
    //    纬度
    private Double latitude;
    //    返回
    private Double wide;
    //    区域类型
    private String areaType;
    //    状态，1.解控（0）2.封控（1）
    private boolean status;
    //    开始时间
    private String startTime;
    //    结束结束
    private String endTime;

    private Integer uId;

    private User userInfo;
}
