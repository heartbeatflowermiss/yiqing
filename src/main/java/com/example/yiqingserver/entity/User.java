package com.example.yiqingserver.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("tb_user")
public class User {
    @TableId(value = "u_id", type = IdType.AUTO)
    private Integer uId;
    //    用户名（学工号）
    private String username;
    //    真实姓名
    private String name;
    //    密码
    private String password;
    //    所示学院
    private String academy;
    //    角色
    private String role;
    //    电话号码
    private String phone;
}
