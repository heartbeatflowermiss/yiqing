package com.example.yiqingserver.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("tb_advice")
public class Advice {

    @TableId(type = IdType.AUTO)
    private Integer id;
    //    用户uid
    private Integer uId;
    //    意见提交时间
    private String submitTime;
    //    意见内容
    private String content;
    //    受理状态，待受理（0） 受理中（1） 已受理（2）
    private Integer status;
    //    意见类型
    private String type;
    //    反馈内容
    private String feedback;

    //    用户——用户连接查询
    private User userInfo;
}
