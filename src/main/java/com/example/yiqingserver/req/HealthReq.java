package com.example.yiqingserver.req;
import lombok.Data;

@Data
public class HealthReq {

    private Integer id;
    //    关联的用户uid
    private Integer uId;
    //    打卡时间
    private String createTime;
    //    是否在校
    private boolean inSchool;
    //    体温
    private String temperature;
    //    异常症状
    private String symptom;
    //    健康码
    private String image;
    //    类型 1.绿码 2.黄码 3.红码
    private String type;
    //    上传状态 1.未上传（0）2.上传（1）
    private boolean status;
    //    备注
    private String remarks;


}
