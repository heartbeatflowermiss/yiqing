package com.example.yiqingserver.req;

import io.swagger.models.auth.In;
import lombok.Data;
@Data
public class UpdateNoticeReq {

    private Integer id;

    private String title;

    private String content;
}
