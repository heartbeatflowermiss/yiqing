package com.example.yiqingserver.req;
import lombok.Data;
@Data
public class FeedBackReq {

    private Integer id;

    private String feedBack;
}
