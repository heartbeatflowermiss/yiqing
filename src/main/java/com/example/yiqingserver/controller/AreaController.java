package com.example.yiqingserver.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.yiqingserver.entity.Area;
import com.example.yiqingserver.mapper.AreaMapper;
import com.example.yiqingserver.req.IsClearance;
import com.example.yiqingserver.service.AreaService;
import com.example.yiqingserver.utils.Result;
import io.swagger.models.auth.In;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("sys-area")
public class AreaController {

    @Resource
    private AreaService areaService;
    @Resource
    private AreaMapper areaMapper;
    /**
     * 不同类型的区域统计
     */
    @GetMapping("areaCensus")
    public Result areaCensus(){
        return areaService.areaCensus();
    }
    /**
     * 返回区域列表-------------------------已测试
     */
    @GetMapping("getAreaList")
    public Result getAreaList(){
//        final List<Area> areas = areaMapper.selectList(null);
//        return new Result<>(200,"成功返回区域列表",areas);
        final List<Area> areaList = areaMapper.getAreaList();
        return new Result<>(200,"这是展示给学校管理员的区域数据",areaList);
    }
    /**
     * 新增封控区域——学校管理员--------------已测试
     */
    @PostMapping("addControlArea")
    public Result addControlArea(@RequestBody Area area){
        return areaService.addControlArea(area);
    }
    /**
     * 更新区域信息-------------已测试
     */
    @PostMapping("updateArea")
    public Result updateArea(@RequestBody Area area){
        areaMapper.updateArea(area);
        return new Result<>(200,"修改区域成功",area);
    }
    /**
     * 封/解控
     */
    @PostMapping("isClearance")
    public Result isClearance(@RequestBody IsClearance isClearance){
        boolean state = isClearance.isStatus();
        areaMapper.isClearance(isClearance);
        if(state){
            return new Result<>(200,"已封控",null);
        }else{
            return new Result<>(200,"已解控",null);
        }
    }
    /**
     * 查询已封控区域
     */
    @GetMapping("controlling")
    public Result controlling(){
//        QueryWrapper wrapper =new QueryWrapper<Area>();
//        wrapper.eq("status",1);
//        final List list = areaMapper.selectList(wrapper);
//        return new Result<>(200,"这是已封控区域信息",list);
        final List<Area> controlling = areaMapper.controlling();
        return new Result<>(200,"这是已封控区域",controlling);
    }
}
