package com.example.yiqingserver.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.yiqingserver.entity.User;
import com.example.yiqingserver.mapper.UserMapper;
import com.example.yiqingserver.req.LoginReq;
import com.example.yiqingserver.service.UserService;
import com.example.yiqingserver.utils.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("sys-user")
public class UserController {

    @Resource
    private UserService userService;

    @Resource
    private UserMapper userMapper;
    /**
     * 登录
     * @param loginReq
     * @return
     */
    @PostMapping("/login")
    public Result login(@RequestBody LoginReq loginReq){
        return userService.login(loginReq);
    }
    /**
     * 根据姓名模糊查询
     */
    @GetMapping("findUserByName")
    public Result findUserByName(String value){
       QueryWrapper wrapper = new QueryWrapper<User>();
       wrapper.like("name",value);
        final List list = userMapper.selectList(wrapper);
        return new Result<>(200,"查找成功",list);
    }
    /**
     * 查询保卫科的人——学校管理员
     */
    @GetMapping("getProtect")
    public Result getProtect(){
        QueryWrapper wrapper = new QueryWrapper<User>();
        wrapper.eq("academy","保卫科");
        wrapper.eq("role","user");
        final List list = userMapper.selectList(wrapper);
        return new Result<>(200,"这是保卫科的人员信息",list);
    }
    /**
     *退出登录
     */
    @GetMapping("logout")
    public Result logout(){
        return new Result<>(200,"退出登录成功",null);
    }
}
