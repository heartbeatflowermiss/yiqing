package com.example.yiqingserver.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.yiqingserver.entity.Notice;
import com.example.yiqingserver.mapper.NoticeMapper;
import com.example.yiqingserver.req.UpdateNoticeReq;
import com.example.yiqingserver.service.NoticeService;
import com.example.yiqingserver.utils.Result;
import io.swagger.models.auth.In;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("sys-notice")
public class NoticeController {

    @Resource
    private NoticeService noticeService;
    @Resource
    private NoticeMapper noticeMapper;

    /**
     * 学校管理员发布学校通知
     * @param notice
     * @return
     */
    @PostMapping("publishNotice")
    public Result publishNotice(@RequestBody Notice notice){
        return noticeService.publishNotice(notice);
    }

    /**
     * 查询所有通知，用于展示在系统首页上
     * @return
     */
    @GetMapping("getAllNotice")
    public Result getAllNotice(){
        final List<Notice> notices = noticeMapper.selectList(null);
        return new Result<>(200,"成功查询到所有通知",notices);
    }
    /**
     * 删除通知信息
     */
    @DeleteMapping("{id}")
    public Result deleteNotice(Integer id){
        QueryWrapper wrapper = new QueryWrapper<>();
        wrapper.eq("id",id);
        noticeMapper.delete(wrapper);
        return new Result<>(200,"删除记录"+id+"成功",null);
    }
    /**
     * 更新通知
     */
    @PostMapping("updateNotice")
    public Result updateNotice(@RequestBody UpdateNoticeReq updateNoticeReq){

        noticeMapper.updateNotice(updateNoticeReq);
        return new Result<>(200,"修改成功",null);
    }
}
