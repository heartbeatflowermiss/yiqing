package com.example.yiqingserver.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.yiqingserver.entity.Health;
import com.example.yiqingserver.mapper.HealthMapper;
import com.example.yiqingserver.req.HealthReq;
import com.example.yiqingserver.service.HealthService;
import com.example.yiqingserver.utils.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("sys-health")
public class HealthController {

    @Resource
    private HealthService healthService;

    @Resource
    private HealthMapper healthMapper;

    /**
     * 健康打卡 || 学院上传相应的信息给学校
     *
     * @param
     * @return
     */
    @PostMapping("clockIn")
    public Result clockIn(@RequestBody Health health) {
        return healthService.clockIn(health);
    }

    /**
     * 展示每个学院的打卡信息(院级)
     *
     * @param academy
     * @return
     */
    @GetMapping("showClockIn")
    public Result showClockIn(String academy) {
//        QueryWrapper wrapper = new QueryWrapper<>();
//        wrapper.eq("academy", academy);
//        final List<Health> health = healthMapper.selectList(wrapper);
//        return new Result<>(200, "查询打卡信息成功", health);
        final List<Health> health = healthMapper.showClockIn(academy);
        return new Result<>(200,"这是展示给学院的打卡信息",health);
    }

    /**
     * 展示所有健康信息
     *
     * @return
     */
    @GetMapping("allClockIn")
    public Result allClockIn() {
        final List<Health> health = healthMapper.allClockIn();
        return new Result<>(200,"这是所有打卡信息",health);
    }

    /**
     * g根据用户uid来查询个人健康打卡历史信息，用于展示在用户个人页面
     *
     * @param uid
     * @return
     */
    @GetMapping("getClockInById")
    public Result getClockInById(Integer uid) {
        final List<Health> health = healthMapper.getHealth(uid);
        System.out.println(health);
        return new Result<>(200,"成功",health);
    }

    /**
     * 学院管理员上传打卡记录，上传状态status-->1
     */
    @PostMapping("uploadToSchool")
    public Result uploadToSchool(Integer idList[]) {
        for (Integer id : idList) {
            healthMapper.uploadToSchool(id);
        }
        return new Result<>(200, "上传健康打卡信息成功", idList);
    }

    /**
     * 学校根据状态status查找健康打卡信息，status = 1则表示已上传
     */
    @GetMapping("getHealthBystatus")
    public Result getHealthBystatus() {
        final List<Health> healthBystatus = healthMapper.getHealthBystatus();
        return new Result<>(200,"这是展示给学校管理员的打卡数据",healthBystatus);
    }

    /**
     * 删除单条健康打卡记录——学校和学院/部门
     */
    @DeleteMapping("delete/{id}")
    public Result delHealth(Integer id) {
        QueryWrapper wrapper = new QueryWrapper<Health>();
        wrapper.eq("id", id);
        healthMapper.delete(wrapper);
        return new Result<>(200, "删除记录" + id + "成功", null);
    }

}
