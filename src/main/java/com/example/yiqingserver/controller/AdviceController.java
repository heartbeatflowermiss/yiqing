package com.example.yiqingserver.controller;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.yiqingserver.entity.Advice;
import com.example.yiqingserver.mapper.AdviceMapper;
import com.example.yiqingserver.req.FeedBackReq;
import com.example.yiqingserver.service.AdviceService;
import com.example.yiqingserver.utils.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("sys-advice")
public class AdviceController {

    @Resource
    private AdviceService adviceService;

    @Resource
    private AdviceMapper adviceMapper;

    /**
     * 发布意见
      * @param advice
     * @return
     */
    @PostMapping("submitAdvice")
    public Result submitAdvice(@RequestBody Advice advice) {
        return adviceService.submitAdvice(advice);
    }
    /**
     * 根据个人uid查询个人发布的意见信息，用于展示在用户个人页面
     * @param uid
     * @return
     */
    @GetMapping("getAdviceById")
    public Result getAdviceById(Integer uid){
//        QueryWrapper wrapper = new QueryWrapper<>();
//        wrapper.eq("u_id",uid);
//        final List<Advice> list = adviceMapper.selectList(wrapper);
//        return new Result<>(200,"查询个人意见信息成功",list);
        final List<Advice> adviceById = adviceMapper.getAdviceById(uid);
        return new Result(200,"这是展示在用户个人界面上的意见数据",adviceById);
    }
    /**
     * 查询所有意见
     * @return
     */
    @GetMapping("getAllAdvice")
    public Result getAllAdvice(){
//        final List<Advice> advice = adviceMapper.selectList(null);
//        System.out.println(advice);
//        return new Result<>(200,"查询所有意见信息成功",advice);
        final List<Advice> allAdvice = adviceMapper.getAllAdvice();
        return new Result<>(200,"这是展示在官网界面上的数据",allAdvice);
    }
    /**
     * 意见信息统计
     */
    @GetMapping("adCensus")
    public Result adCensus(){
        return adviceService.adCensus();
    }
    /**
     * 意见类型统计
     */
    @GetMapping("typeCensus")
    public Result typeCensus(){
        return adviceService.typeCensus();
    }
    /**
     * 接收意见，将受理状态从0->1
     */
    @PostMapping("receive")
    public Result receive(Integer id){
        QueryWrapper wrapper = new QueryWrapper<>();
        wrapper.eq("id",id);
        final Advice advice = adviceMapper.selectOne(wrapper);
        if(advice.getStatus() == 1){
            return new Result<>(401,"已接收意见，请勿重复接收",null);
        }else {
            adviceMapper.receive(id);
            return new Result<>(200,"成功接收",null);
        }
    }
    /**
     * 反馈意见，状态status从1->2,并填写反馈信息
     */
    @PostMapping("feedBack")
    public Result feedBack(@RequestBody FeedBackReq feedBackReq){
        adviceMapper.feedBack(feedBackReq);
        return new Result<>(200,"意见反馈成功",feedBackReq);
    }
}
